package com.citycar.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;
import java.util.List;

public class Product {
    private Integer id;
    private String description;
    private String name;
    private Float price;
    private Integer qty;
    private Boolean status;
    @JsonProperty(value = "total_view")
    private Integer totalView;
    @JsonProperty(value = "expire_advertise")
    private Timestamp expireAdvertise;
    @JsonProperty(value = "expire_date")
    private Timestamp expireDate;
    @JsonProperty(value = "created_date")
    private Timestamp createdDate;
    @JsonProperty(value = "modified_date")
    private Timestamp modifiedDate;
    @JsonProperty(value = "is_expire")
    private Boolean isExpire;
    @JsonProperty(value = "is_advertise")
    private Boolean isAdvertise;
    private String url;
    @JsonProperty(value = "category_id")
    private Integer categoryId;
    @JsonProperty(value = "user_id")
    private Integer userId;
    @JsonProperty(value = "total_count")
    private Integer totalCount;
    @JsonProperty(value = "add_to_card")
    private Integer addToCard;

    // create category
    private Category category;
    // create user
    private User user;

    private List<ProductImage> productImages;

    public Product() {
    }

    public Integer getAddToCard() {
        return addToCard;
    }

    public void setAddToCard(Integer addToCard) {
        this.addToCard = addToCard;
    }

    public Product(Integer id, String description, String name, Float price, Integer qty, Boolean status, Integer totalView, Timestamp expireAdvertise, Timestamp expireDate, Timestamp createdDate, Timestamp modifiedDate, Integer categoryId, Integer userId) {
        this.id = id;
        this.description = description;
        this.name = name;
        this.price = price;
        this.qty = qty;
        this.status = status;
        this.totalView = totalView;
        this.expireAdvertise = expireAdvertise;
        this.expireDate = expireDate;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.categoryId = categoryId;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getTotalView() {
        return totalView;
    }

    public void setTotalView(Integer totalView) {
        this.totalView = totalView;
    }

    public Timestamp getExpireAdvertise() {
        return expireAdvertise;
    }

    public void setExpireAdvertise(Timestamp expireAdvertise) {
        this.expireAdvertise = expireAdvertise;
    }

    public Timestamp getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Timestamp expireDate) {
        this.expireDate = expireDate;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Boolean getExpire() {
        return isExpire;
    }

    public void setExpire(Boolean expire) {
        isExpire = expire;
    }

    public Boolean getAdvertise() {
        return isAdvertise;
    }

    public void setAdvertise(Boolean advertise) {
        isAdvertise = advertise;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }


    public List<ProductImage> getProductImages() {
        return productImages;
    }

    public void setProductImages(List<ProductImage> productImages) {
        this.productImages = productImages;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", qty=" + qty +
                ", status=" + status +
                ", totalView=" + totalView +
                ", expireAdvertise=" + expireAdvertise +
                ", expireDate=" + expireDate +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", isExpire=" + isExpire +
                ", isAdvertise=" + isAdvertise +
                ", url='" + url + '\'' +
                ", categoryId=" + categoryId +
                ", userId=" + userId +
                ", totalCount=" + totalCount +
                ", category=" + category +
                ", user=" + user +
                ", productImages=" + productImages +
                '}';
    }
}
