package com.citycar.model.message;

import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public final class JavaUtil {

    public static final String MESSAGE_SUCCESS="SUCCESS";
    public static final String MESSAGE_UN_SUCCESS="UNSUCCESS";

    public static final String STATUS_SUCCESS="0000";
    public static final String TATUS_UN_SUCCESS="1111";

    public static final String STRING_STATUS="STATUS";
    public static final String STRING_MESSAGE="MSG";
    public static final String STRING_REC="REC";

    public static final String STRING_PAGENATION="PAGENATION";


    public static void addTextWatermark(String text, String type, MultipartFile source, File destination) throws IOException {
        BufferedImage image = ImageIO.read(source.getInputStream());

        // determine image type and handle correct transparency
        int imageType = "png".equalsIgnoreCase(type) ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
        BufferedImage watermarked = new BufferedImage(image.getWidth(), image.getHeight(), imageType);

        // initializes necessary graphic properties
        Graphics2D w = (Graphics2D) watermarked.getGraphics();
        w.drawImage(image, 0, 0, null);
        AlphaComposite alphaChannel = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f);
        w.setComposite(alphaChannel);
        w.setColor(Color.GRAY);
        w.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 35));
        FontMetrics fontMetrics = w.getFontMetrics();
        Rectangle2D rect = fontMetrics.getStringBounds(text, w);

        // calculate center of the image
        int centerX = (image.getWidth() - (int) rect.getWidth()) / 2;
        int centerY = image.getHeight() / 2;

        // add text overlay to the image
        w.drawString(text, centerX, centerY);
        ImageIO.write(watermarked, type, destination);
        w.dispose();
    }


}
