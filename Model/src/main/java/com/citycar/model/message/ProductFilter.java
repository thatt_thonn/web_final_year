package com.citycar.model.message;

public class ProductFilter {
    private Integer id;
    private String name;
    private Integer categoryId;
    private Integer userId;
    private Integer wishlistUserId;


    public ProductFilter() {
    }

    public ProductFilter(Integer id, String name, Integer categoryId, Integer userId, Integer wishlistUserId) {
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.userId = userId;
        this.wishlistUserId = wishlistUserId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getWishlistUserId() {
        return wishlistUserId;
    }

    public void setWishlistUserId(Integer wishlistUserId) {
        this.wishlistUserId = wishlistUserId;
    }



    @Override
    public String toString() {
        return "ProductFilter{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", categoryId=" + categoryId +
                ", userId=" + userId +
                ", wishlistUserId=" + wishlistUserId +
                '}';
    }
}


