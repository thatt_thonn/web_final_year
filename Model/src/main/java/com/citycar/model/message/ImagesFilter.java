package com.citycar.model.message;

public class ImagesFilter {
    private Integer id;
    private String filename;
    private String originalFilename;
    private String uuid;
    private String url;
    private Integer userId;

    public ImagesFilter() {
    }

    public ImagesFilter(Integer id, String filename, String originalFilename, String uuid, String url, Integer userId) {
        this.id = id;
        this.filename = filename;
        this.originalFilename = originalFilename;
        this.uuid = uuid;
        this.url = url;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getOriginalFilename() {
        return originalFilename;
    }

    public void setOriginalFilename(String originalFilename) {
        this.originalFilename = originalFilename;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
