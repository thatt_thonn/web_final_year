package com.citycar.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;
import java.util.List;

public class Invoice {
    private Integer id;
    private Timestamp date;
    @JsonProperty(value = "payment_id")
    private Integer paymentId;
    @JsonProperty(value = "total_amount")
    private Float totalAmount;
    private Boolean status;
    @JsonProperty(value = "created_date")
    private Timestamp createdDate;
    @JsonProperty(value = "modified_date")
    private Timestamp modifiedDate;
    @JsonProperty(value = "user_id")
    private Integer userId;

    // invoice detail
    private List<InvoiceDetail> invoiceDetails;


    public Invoice() {
    }

    public Invoice(Integer id, Timestamp date, Integer paymentId, Float totalAmount, Boolean status, Timestamp createdDate, Timestamp modifiedDate, Integer userId) {
        this.id = id;
        this.date = date;
        this.paymentId = paymentId;
        this.totalAmount = totalAmount;
        this.status = status;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(Integer paymentId) {
        this.paymentId = paymentId;
    }

    public Float getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Float totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<InvoiceDetail> getInvoiceDetails() {
        return invoiceDetails;
    }

    public void setInvoiceDetails(List<InvoiceDetail> invoiceDetails) {
        this.invoiceDetails = invoiceDetails;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", date=" + date +
                ", paymentId=" + paymentId +
                ", totalAmount=" + totalAmount +
                ", status=" + status +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                ", userId=" + userId +
                ", invoiceDetails=" + invoiceDetails +
                '}';
    }
}
