package com.citycar.model;

public class ProductImage {
    private Integer id;
    private String url;
    private Integer priority;
    private Integer productId;

    public ProductImage() {
    }

    public ProductImage(Integer id, String url, Integer priority, Integer productId) {
        this.id = id;
        this.url = url;
        this.priority = priority;
        this.productId = productId;
    }

    public ProductImage(String url, Integer priority, Integer productId) {
        this.url = url;
        this.priority = priority;
        this.productId = productId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    @Override
    public String toString() {
        return "ProductImage{" +
                "id=" + id +
                ", url='" + url + '\'' +
                ", priority=" + priority +
                ", productId=" + productId +
                '}';
    }
}
