package com.citycar.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

public class PaymentTransaction {
    private Integer id;
    @JsonProperty(value = "payment_type")
    private String paymentType;
    @JsonProperty(value = "auth_user_id")
    private String authUserId;
    @JsonProperty(value = "auth_name")
    private String authName;
    private String desc;
    @JsonProperty(value = "created_date")
    private Timestamp createdDate;
    @JsonProperty(value = "modified_date")
    private Timestamp modifiedDate;

    public PaymentTransaction() {
    }

    public PaymentTransaction(Integer id, String paymentType, String authUserId, String authName, String desc, Timestamp createdDate, Timestamp modifiedDate) {
        this.id = id;
        this.paymentType = paymentType;
        this.authUserId = authUserId;
        this.authName = authName;
        this.desc = desc;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getAuthUserId() {
        return authUserId;
    }

    public void setAuthUserId(String authUserId) {
        this.authUserId = authUserId;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    @Override
    public String toString() {
        return "PaymentTransaction{" +
                "id=" + id +
                ", paymentType='" + paymentType + '\'' +
                ", authUserId='" + authUserId + '\'' +
                ", authName='" + authName + '\'' +
                ", desc='" + desc + '\'' +
                ", createdDate=" + createdDate +
                ", modifiedDate=" + modifiedDate +
                '}';
    }
}
