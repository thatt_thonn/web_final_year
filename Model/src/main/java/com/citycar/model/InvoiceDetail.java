package com.citycar.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;

public class InvoiceDetail {

    private Integer id;
    private Timestamp date;
    @JsonProperty(value = "product_id")
    private Integer productId;
    private Float amount;
    @JsonProperty(value = "invoice_id")
    private Integer invoiceId;
    private Boolean status;
    @JsonProperty(value = "created_date")
    private Timestamp createdDate;
    @JsonProperty(value = "modified_date")
    private Timestamp modifiedDate;
    private Integer qty;
    private String name;
    private String url;
    private Float price;
    private Integer userId;



    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public InvoiceDetail() {
    }

    public InvoiceDetail(Integer id, Timestamp date, Integer productId, Float amount, Integer invoiceId, Boolean status, Timestamp createdDate, Timestamp modifiedDate) {
        this.id = id;
        this.date = date;
        this.productId = productId;
        this.amount = amount;
        this.invoiceId = invoiceId;
        this.status = status;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

}
