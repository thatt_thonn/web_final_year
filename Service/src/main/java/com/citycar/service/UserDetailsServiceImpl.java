package com.citycar.service;

import com.citycar.model.User;
import com.citycar.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);
    private User user;
    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        user=userRepository.getUserByUsername(s);
        System.out.println("userDetail"+user);
        if(user==null){
            throw  new UsernameNotFoundException("User not authorized...");
        }
        return user;
    }


    public Integer saveUser(User user){
        return userRepository.saveUser(user);
    }


}
