package com.citycar.service;

import com.citycar.model.WishList;
import com.citycar.repository.WishlistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MyWishlistService {

    private WishlistRepository wishlistRepository;

    @Autowired
    public void setWishlistRepository(WishlistRepository wishlistRepository) {
        this.wishlistRepository = wishlistRepository;
    }

    public Integer createWishlist(WishList wishList) {
        Integer status = wishlistRepository.countCheckWishlist(wishList);
        Integer removeId = null, createId = null;
        Integer resutlId=0;
        if (status > 0) {
            // remove them
            removeId = wishlistRepository.removeWishlist(wishList);
            if(removeId>0){
                resutlId=1;
            }
        } else {
            // add them
            createId = wishlistRepository.createWishlist(wishList);
            if(createId>0){
                resutlId=2;
            }
        }
        return resutlId;
    }


    public Integer countAllWishListUser(Integer id){
        return wishlistRepository.countAllWishListUser(id);
    }
}
