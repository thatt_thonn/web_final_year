package com.citycar.service;

import com.citycar.model.InvoiceDetail;
import com.citycar.repository.MyCardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyCardService {
    private MyCardRepository myCardRepository;

    @Autowired
    public void setMyCardRepository(MyCardRepository myCardRepository) {
        this.myCardRepository = myCardRepository;
    }

    public List<InvoiceDetail> getAllCard(Integer userId) {
        return myCardRepository.getAllCard(userId);
    }

    public Integer updateCardInvoiceIdAndStatus(InvoiceDetail invoiceDetail) {
        return myCardRepository.updateCardInvoiceIdAndStatus(invoiceDetail);
    }

    public Integer addToCard(InvoiceDetail invoiceDetail) {
        Integer checkExistingData = myCardRepository.checkExistingCard(invoiceDetail);
        Integer updateId = 0, createId = 0;
        Integer result = 0;
        if (checkExistingData > 0) {
            if (myCardRepository.updateAmountAndQty(invoiceDetail) > 0) {
                result = 1;
            }
        } else {
            if (myCardRepository.addToCard(invoiceDetail) > 0) {
                result = 1;
            }
        }
        return result;
    }

    public Float totalPriceCard(Integer userId) {
        return myCardRepository.totalPriceCard(userId);
    }
}
