package com.citycar.service;

import com.citycar.repository.RoleRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private RoleRepositoy roleRepositoy;

    @Autowired
    public void setRoleRepositoy(RoleRepositoy roleRepositoy) {
        this.roleRepositoy = roleRepositoy;
    }

    public Integer getRoleByName(String name){
        return roleRepositoy.getRoleByName(name);
    }
}
