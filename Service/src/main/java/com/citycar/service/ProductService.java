package com.citycar.service;


import com.citycar.model.Product;
import com.citycar.model.message.Pagination;
import com.citycar.model.message.ProductFilter;
import com.citycar.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> getAllProduct(Pagination pagination, ProductFilter productFilter) throws Exception{
        return productRepository.getAllProduct(pagination,productFilter);
    }


    public List<Product> getAllProductAdmin(){
        return productRepository.getAllProductAdmin();
    }



    public Integer saveProduct(Product product){
        return productRepository.saveProduct(product);
    }

    public Integer deleteProductById(Integer id){
        return productRepository.deleteProductById(id);
    }

    public Integer updateTotalViewProduct(Integer id){
        return productRepository.updateTotalView(id);
    }
}
