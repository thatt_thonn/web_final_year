package com.citycar.service;

import com.citycar.model.Category;
import com.citycar.repository.CategoryRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService {

    private CategoryRepositoy categoryRepositoy;

    @Autowired
    public void setCategoryRepositoy(CategoryRepositoy categoryRepositoy) {
        this.categoryRepositoy = categoryRepositoy;
    }

    public List<Category> getAllCategory()throws Exception{
        return categoryRepositoy.getAllCategory();
    }

    public Category getCategoryById(Integer id){
        return categoryRepositoy.getCategoryById(id);
    }

    public Category createCategory(Category category)throws Exception{
        return categoryRepositoy.createCategory(category);
    }


    public Integer updateCategory(Category category) throws Exception{
        return categoryRepositoy.updateCategory(category);
    }


    public Integer deleteCategory(Integer id) throws Exception{
        return categoryRepositoy.deleteCategory(id);
    }


}
