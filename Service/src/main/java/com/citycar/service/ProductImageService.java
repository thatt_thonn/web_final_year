package com.citycar.service;

import com.citycar.model.ProductImage;
import com.citycar.model.message.ImagesFilter;
import com.citycar.repository.ProductImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductImageService {

    private ProductImageRepository productImageRepository;

    @Autowired
    public void setProductImageRepository(ProductImageRepository productImageRepository) {
        this.productImageRepository = productImageRepository;
    }

    public Integer saveProductImage(ProductImage productImage) throws Exception {
        return productImageRepository.saveProductImage(productImage);
    }

    public Integer modifyProductImageById(ProductImage productImage) {
        try {
            return productImageRepository.modifyProductImageById(productImage);
        } catch (Exception e) {
            return 0;
        }
    }

    public Integer deleteProductImageById(Integer id) {
        try {
            return productImageRepository.deleteProductImageById(id);
        } catch (Exception e) {
            return 0;
        }
    }

    public String findUrlNameById(Integer id)throws Exception{
        return productImageRepository.findUrlNameById(id);
    }

    public List<ProductImage> getAllProductImageByProductID(Integer id) throws Exception{
        return productImageRepository.getAllProductImageByProductID(id);
    }

    public Integer saveTempImage(ImagesFilter imagesFilter) throws Exception
    {
        return productImageRepository.saveTempImage(imagesFilter);
    }
    public String removeTempImage(String uuid) throws Exception{
        return productImageRepository.removeTempImage(uuid);
    }

    public List<ImagesFilter> getAllTempImageByUserId(Integer userId)throws Exception{
        return productImageRepository.getAllTempImageByUserId(userId);
    }

    public Integer deleteAllTempImage(Integer userId)throws Exception{
        return productImageRepository.deleteAllTempImage(userId);
    }
}
