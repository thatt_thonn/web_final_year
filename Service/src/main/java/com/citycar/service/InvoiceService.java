package com.citycar.service;


import com.citycar.model.Invoice;
import com.citycar.repository.InvoiceRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvoiceService {

    private InvoiceRepositoy invoiceRepositoy;

    @Autowired
    public void setInvoiceRepositoy(InvoiceRepositoy invoiceRepositoy) {
        this.invoiceRepositoy = invoiceRepositoy;
    }

    public Invoice getInvoiceByUser(Integer id){
        return invoiceRepositoy.getInvoiceByUserId(id);
    }
}
