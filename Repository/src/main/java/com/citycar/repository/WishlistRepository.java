package com.citycar.repository;

import com.citycar.model.WishList;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface WishlistRepository {

    @Insert("INSERT INTO public.e_whish_list(product_id, user_id)VALUES(#{wishList.productId}, #{wishList.userId});")
    public Integer createWishlist(@Param("wishList") WishList wishList);

    @Select("select count(*) from e_whish_list where product_id=#{wishList.productId} and user_id=#{wishList.userId}")
    public Integer countCheckWishlist(@Param("wishList") WishList wishList);

    @Delete("delete from e_whish_list where product_id=#{wishList.productId} and user_id=#{wishList.userId}")
    public Integer removeWishlist(@Param("wishList") WishList wishList);


    @Select("select count(*) from e_whish_list where user_id=#{id}")
    public Integer countAllWishListUser(@Param("id")Integer id);


}
