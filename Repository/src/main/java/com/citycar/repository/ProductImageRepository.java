package com.citycar.repository;


import com.citycar.model.ProductImage;
import com.citycar.model.message.ImagesFilter;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductImageRepository {

    @Insert("INSERT INTO e_product_image(url, priority,product_id) VALUES(#{productImage.url},#{productImage.priority},#{productImage.productId});")
    public Integer saveProductImage(@Param("productImage") ProductImage productImage);

    @Update("UPDATE e_product_image SET url=#{productImage.url} WHERE id=#{productImage.id}")
    public Integer modifyProductImageById(@Param("productImage") ProductImage productImage);

    @Delete("DELETE FROM e_product_image WHERE id=#{id}")
    public Integer deleteProductImageById(@Param("id") Integer id);

    @Select("SELECT url FROM e_product_image WHERE id=#{id}")
    public String findUrlNameById(@Param("id") Integer id);


    @Select("SELECT pi.id,pi.url FROM e_product_image pi where pi.product_id=#{id};")
    public List<ProductImage> getAllProductImageByProductID(@Param("id") Integer id);


    @Insert("INSERT INTO c_file_temp(filename,original_filename,uuid,url,user_id)\n" +
            "VALUES(#{imagesFilter.filename},#{imagesFilter.originalFilename},#{imagesFilter.uuid},#{imagesFilter.url},#{imagesFilter.userId});")
    public Integer saveTempImage(@Param("imagesFilter") ImagesFilter imagesFilter);

    @Select("delete from c_file_temp where uuid=#{uuid} returning filename;")
    public String removeTempImage(@Param("uuid") String uuid);

    @Select("SELECT id, filename, original_filename as originalFilename, uuid, url, user_id as userId FROM c_file_temp where user_id=#{userId}")
    public List<ImagesFilter> getAllTempImageByUserId(@Param("userId") Integer userId);

    @Delete("DELETE FROM c_file_temp WHERE user_id=#{userId}")
    public Integer deleteAllTempImage(@Param("userId") Integer userId);


}
