package com.citycar.repository;

import com.citycar.model.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepositoy {

    @Select("SELECT  \n" +
            "            ca.id,  \n" +
            "            ca.name,  \n" +
            "            ca.created_date as createdDate,  \n" +
            "            ca.modified_date as modifiedDate,  \n" +
            "            ca.user_id as userId, \n" +
            "            (select count(*) from e_product where category_id=ca.id) as totalProduct \n" +
            "            FROM e_category ca order by ca.id desc")
    public List<Category> getAllCategory();


    @Select("SELECT \n" +
            "ca.id, \n" +
            "ca.name, \n" +
            "ca.created_date as createdDate, \n" +
            "ca.modified_date as modifiedDate, \n" +
            "ca.user_id as userId,\n" +
            "(select count(*) from e_product where category_id=ca.id) as totalProduct\n" +
            "FROM public.e_category ca where ca.id=#{id};")
    public Category getCategoryById(@Param("id") Integer id);


    @Select("INSERT INTO public.e_category\n" +
            "(name, created_date, modified_date, user_id)\n" +
            "VALUES(#{category.name}, now(), now(), #{category.userId}) returning " +
            "id,name," +
            "modified_date as modifiedDate," +
            "created_date as createdDate," +
            "user_id as userId;")
    public Category createCategory(@Param("category") Category category);


    @Update("UPDATE public.e_category\n" +
            "SET name=#{name}, modified_date=now(), user_id=#{userId}\n" +
            "WHERE id=#{id};")
    public Integer updateCategory(Category category);

    @Delete("DELETE FROM public.e_category\n" +
            "WHERE id=#{id};")
    public Integer deleteCategory(@Param("id") Integer id);


}
