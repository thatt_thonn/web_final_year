package com.citycar.repository;

import com.citycar.model.Invoice;
import com.citycar.model.InvoiceDetail;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvoiceRepositoy {

    @Select("select\n" +
            "\tid,\n" +
            "\t\"date\",\n" +
            "\tuser_id as userId,\n" +
            "\tpayment_id as paymentId,\n" +
            "\t(select sum(ie.amount) from e_invoice_detail as ie where ie.invoice_id=id) totalAmount,\n" +
            "\tstatus,\n" +
            "\tcreated_date as createdDate,\n" +
            "\tmodified_date as modifiedDate\n" +
            "from\n" +
            "\tpublic.e_invoice where status=false and user_id=#{id};")
    @Results({
            @Result(id = true,property = "id", column = "id"),
            @Result(property="invoiceDetails", column="id", many=@Many(select="getInvoiceDetail"))
    })
    public Invoice getInvoiceByUserId(@Param("id") Integer id);


    @Select("select\n" +
            "\tie.id,\n" +
            "\tie.\"date\",\n" +
            "\tie.product_id productId,\n" +
            "\tie.created_date createdDate, \n" +
            "\tie.modified_date modifiedDate,\n" +
            "\tie.amount,\n" +
            "\tie.qty,\n" +
            "\tie.invoice_id invoiceId,\n" +
            "\tie.status,\n" +
            "\tpr.name,pr.price,\n" +
            "\t(SELECT pi.url FROM e_product_image pi WHERE pi.product_id=pr.\"id\" LIMIT 1) as url\n" +
            "from e_product pr\n" +
            "inner join e_invoice_detail as ie on ie.product_id=pr.\"id\"\n" +
            "where invoice_id=#{id};")
    public List<InvoiceDetail> getInvoiceDetail(@Param("id") Integer id);
}
