package com.citycar.repository;

import com.citycar.model.InvoiceDetail;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MyCardRepository {

    @Insert("insert\n" +
            "\tinto\n" +
            "\t\tpublic.e_invoice_detail ( product_id,\n" +
            "\t\tamount,\n" +
            "\t\tstatus,\n" +
            "\t\tqty,\n" +
            "\t\tuser_id)\n" +
            "\tvalues( #{invoiceDetail.productId},\n" +
            "\t#{invoiceDetail.amount},\n" +
            "\t#{invoiceDetail.status},\n" +
            "\t#{invoiceDetail.qty},\n" +
            "\t#{invoiceDetail.userId});\n")
    public Integer addToCard(@Param("invoiceDetail") InvoiceDetail invoiceDetail);

    @Select("select count(*) from e_invoice_detail where user_id=#{invoiceDetail.userId} and product_id=#{invoiceDetail.productId} and status=false")
    public Integer checkExistingCard(@Param("invoiceDetail") InvoiceDetail invoiceDetail);

    @Update("update e_invoice_detail set qty=qty+1, amount=amount+amount where user_id=#{invoiceDetail.userId} and product_id=#{invoiceDetail.productId} and status=false")
    public Integer updateAmountAndQty(@Param("invoiceDetail") InvoiceDetail invoiceDetail);


    @Update("update\n" +
            "\te_invoice_detail\n" +
            "set\n" +
            "\t\"date\" = now(),\n" +
            "\tcreated_date = now(),\n" +
            "\tmodified_date = now(),\n" +
            "\tinvoice_id = #{invoiceDetail.invoiceId},\n" +
            "\tstatus = #{invoiceDetail.status},\n" +
            "\tuser_id = #{invoiceDetail.userId}\n" +
            "where\n" +
            "\tuser_id=#{invoiceDetail.userId} and status=#{invoiceDetail.status}")
    public Integer updateCardInvoiceIdAndStatus(@Param("invoiceDetail") InvoiceDetail invoiceDetail);


    @Select("select\n" +
            "\tie.id,\n" +
            "\tie.\"date\",\n" +
            "\tie.product_id as productId,\n" +
            "\tie.created_date as createdDate,\n" +
            "\tie.modified_date as modifiedDate,\n" +
            "\tie.amount,\n" +
            "\tie.invoice_id as invoiceId,\n" +
            "\tie.status,\n" +
            "\tie.qty,\n" +
            "\tie.user_id as userId,\n" +
            "\tpr.name,\n" +
            "\t(SELECT pi.url FROM e_product_image pi WHERE pi.product_id=pr.\"id\" LIMIT 1) as url,\n" +
            "\tpr.price\n" +
            "from\n" +
            "\tpublic.e_invoice_detail ie\n" +
            "inner join e_product pr on pr.id=ie.product_id\n" +
            "where ie.user_id=#{userId} and ie.status=false")
    public List<InvoiceDetail> getAllCard(@Param("userId") Integer userId);

    @Select("select sum(amount) from e_invoice_detail where user_id=#{userId} and status=false")
    public Float totalPriceCard(@Param("userId") Integer userId);


    @Select("select count(*) from e_invoice_detail where user_id=#{userId} and status=false")
    public Integer totalData(@Param("userId")Integer userId);



}
