package com.citycar.repository.dynamic;

import com.citycar.model.User;
import com.citycar.model.message.Pagination;
import com.citycar.model.message.ProductFilter;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class ProductRepositoryDynamic {

    private static final Logger logger = LoggerFactory.getLogger(ProductRepositoryDynamic.class);


    Authentication authentication = null;
    Integer authen_user_id=0;

    public String getAllProduct(@Param("pagination") Pagination pagination, @Param("productFilter") ProductFilter productFilter) {
        return new SQL() {{

            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            if (!auth.getPrincipal().equals("anonymousUser")) {
                authentication = SecurityContextHolder.getContext().getAuthentication();
                Object principle = authentication.getPrincipal();
                authen_user_id = ((User) principle).getId();
            }

            String sql = " (count(pr.id) over(PARTITION BY pr.status)) as totalCount,  \n" +
                    "pr.id,   \n" +
                    "pr.create_date as createdDate,   \n" +
                    "pr.description,   \n" +
                    "pr.expire_advertise as expireAdvertise,   \n" +
                    "pr.expire_date as expireDate,   \n" +
                    "pr.modified_date as modifiedDate,   \n" +
                    "pr.name,   \n" +
                    "pr.price,   \n" +
                    "pr.qty,   \n" +
                    "pr.status,   \n" +
                    "pr.category_id as categoryId,   \n" +
                    "pr.users_id as userId,   \n" +
                    "pr.total_view as totalView, (SELECT count(*) from e_whish_list WHERE product_id=pr.id and user_id="+authen_user_id+") as addToCard, \n" +
                    "pr.expire_date>=CURRENT_TIMESTAMP as isExpire,  \n" +
                    "(CASE WHEN pr.expire_advertise>NOW() THEN TRUE ELSE FALSE END) AS isAdvertise,  \n" +
                    "(SELECT pi.url FROM e_product_image pi WHERE pi.product_id=pr.id LIMIT 1) as url  FROM public.e_product pr";

            if (productFilter.getWishlistUserId() != 0) {
                sql += "\n INNER JOIN e_whish_list wl on wl.product_id=pr.id";
            }
            sql += "\nwhere 1=1 ";

            if (productFilter.getWishlistUserId() != 0) {
                sql += "\n AND wl.user_id = " + productFilter.getWishlistUserId();
            }

            if (productFilter.getId() != 0) {
                sql += "\n AND pr.id = " + productFilter.getId();
            }

            if (productFilter.getUserId() != 0) {
                sql += "\n AND pr.users_id = " + productFilter.getUserId();
            }

            if (productFilter.getCategoryId() != 0) {
                sql += "\n AND pr.category_id = " + productFilter.getCategoryId();
            }

            if (!"".equals(productFilter.getName())) {
                sql += "\n AND pr.name ILIKE '%' || " + productFilter.getName() + " || '%'";
            }

            sql+="\nORDER BY pr.id desc";

            if (pagination.getLimit() > 0 && pagination.getPage() > 0) {
                sql += "\n LIMIT " + pagination.getLimit() + " OFFSET " + pagination.getOffset();
            }
            SELECT(sql);
            logger.info(sql);
        }}.toString();
    }


}
