package com.citycar.repository;

import com.citycar.model.Role;
import com.citycar.model.User;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {


    @Select("select\n" +
            "\tid,\n" +
            "\tfirst_name as firstName,\n" +
            "\tlast_name as lastName,\n" +
            "\temail,\n" +
            "\tphone,\n" +
            "\t\"password\",\n" +
            "\tgender,\n" +
            "\tstatus,\n" +
            "\tcreated_date as createdDate,\n" +
            "\tmodified_date as modifiedDate,\n" +
            "\tprofile,role_id\n" +
            "from\n" +
            "\tpublic.e_user where email=#{email};\n")
    @Results({
            @Result(id=true, property = "id", column = "id"),
            @Result(property = "roles", column = "role_id", many = @Many(select = "findRoleByRoleId")),
    })
    public User getUserByUsername(String email);



    @Select("SELECT\n" +
            "\t\trl.id,\n" +
            "\t\trl.name,\n" +
            "\t\trl.created_date AS createdDate,\n" +
            "\t\trl.modified_date AS modifiedDate,\n" +
            "\t\trl.description\n" +
            "FROM e_role rl\n" +
            "WHERE 1=1\n" +
            "AND rl.id=#{id}")
    public List<Role> findRoleByRoleId(Integer id);


    @Insert("INSERT INTO public.e_user\n" +
            "(first_name, last_name, email, phone, \"password\", gender, status, created_date, modified_date, role_id, profile)\n" +
            "VALUES(#{user.firstName}, #{user.lastName}, #{user.email}, #{user.phone}, #{user.password}, null, true, now(), now(), #{user.roleId}, '/client/img/user.png');")
    public Integer saveUser(@Param("user")User user);

}
