package com.citycar.repository;

import com.citycar.model.Category;
import com.citycar.model.Product;
import com.citycar.model.ProductImage;
import com.citycar.model.User;
import com.citycar.model.message.Pagination;
import com.citycar.model.message.ProductFilter;
import com.citycar.repository.dynamic.ProductRepositoryDynamic;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository {
    @SelectProvider(method = "getAllProduct",type = ProductRepositoryDynamic.class)
    @Results({
            @Result(id = true,property = "id", column = "id"),
            @Result(property="category", column="categoryId", one=@One(select="getCategoryById")),
            @Result(property="user", column="userId", one=@One(select="getUser")),
            @Result(property="productImages", column="id", many=@Many(select="getProductImage"))
    })
    public List<Product> getAllProduct(@Param("pagination")Pagination pagination,@Param("productFilter") ProductFilter productFilter);


    @Select("SELECT \n" +
            "ca.id, \n" +
            "ca.name, \n" +
            "ca.created_date as createdDate, \n" +
            "ca.modified_date as modifiedDate, \n" +
            "ca.user_id as userId,\n" +
            "(select count(*) from e_product where category_id=ca.id) as totalProduct\n" +
            "FROM public.e_category ca where ca.id=#{id};")
    public Category getCategoryById(Integer id);

    @Select("SELECT id, url,priority, product_id as productId\n" +
            "FROM public.e_product_image where product_id=#{id};")
    public List<ProductImage> getProductImage(Integer id);


    @Select("SELECT \n" +
            "id, \n" +
            "first_name as firstName, \n" +
            "last_name as lastName, \n" +
            "email, \n" +
            "phone, \n" +
            "\"password\", \n" +
            "gender, \n" +
            "status, \n" +
            "created_date as createdDate,  \n" +
            "modified_date as modifiedDate, \n" +
            "role_id as roleId, \n" +
            "profile\n" +
            "FROM public.e_user where id=#{id};")
    public User getUser(Integer id);


    @Select("SELECT  \n" +
            "count(*)\n" +
            "FROM public.e_product pr\n" +
            "INNER JOIN e_whish_list wl on wl.product_id=pr.id\n" +
            "where wl.user_id = #{authUserId}")
    public Integer countAllMyWishlist(@Param("authUserId") Integer authUserId);


    @Select("select pr.id,   \n"+
                   "pr.create_date as createdDate,   \n"+
                   "pr.description,   \n"+
                   "pr.expire_advertise as expireAdvertise,   \n"+
                   "pr.expire_date as expireDate,   \n"+
                   "pr.modified_date as modifiedDate,   \n"+
                   "pr.name,   \n"+
                   "pr.price,   \n"+
                   "pr.qty,   \n"+
                   "pr.status,   \n"+
                   "pr.category_id as categoryId,   \n"+
                   "pr.users_id as userId,   \n"+
                   "pr.total_view as totalView,  \n"+
                   "pr.expire_date>=CURRENT_TIMESTAMP as isExpire,  \n"+
                   "(CASE WHEN pr.expire_advertise>NOW() THEN TRUE ELSE FALSE END) AS isAdvertise,  \n"+
                   "(SELECT pi.url FROM e_product_image pi WHERE pi.product_id=pr.id LIMIT 1) as url  FROM public.e_product pr\n"+
                   "where 1=1 ")
    @Results({
            @Result(id = true,property = "id", column = "id"),
            @Result(property="category", column="categoryId", one=@One(select="getCategoryById")),
            @Result(property="user", column="userId", one=@One(select="getUser")),
            @Result(property="productImages", column="id", many=@Many(select="getProductImage"))
    })
    public List<Product> getAllProductAdmin();


    @Delete("delete from e_product where id=#{id}")
    public Integer deleteProduct(@Param("id")Integer id);

    @Select("INSERT INTO public.e_product\n" +
            "(create_date, description, expire_advertise, expire_date, modified_date, name, price, qty, status, category_id, users_id, total_view)\n" +
            "VALUES(now(), '', now()+interval '3 day', now()+interval '3 month', now(), #{product.name}, #{product.price}, #{product.qty}, true, #{product.category.id}, #{product.user.id}, #{product.totalView}) RETURNING id;")
    public Integer saveProduct(@Param("product") Product product);


    @Delete("delete from e_product where id=#{id}")
    public Integer deleteProductById(@Param("id")Integer id);


    @Update("update e_product set total_view=total_view+1 where id=#{id}")
    public Integer updateTotalView(@Param("id") Integer id);



}
