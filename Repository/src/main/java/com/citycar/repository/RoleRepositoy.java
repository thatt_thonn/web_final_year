package com.citycar.repository;


import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepositoy {

    @Select("SELECT id FROM e_role WHERE name=#{name}")
    public Integer getRoleByName(@Param("name") String name);

}
