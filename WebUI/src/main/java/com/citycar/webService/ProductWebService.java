package com.citycar.webService;

import com.citycar.model.Product;
import com.citycar.model.message.JavaUtil;
import com.citycar.model.message.Pagination;
import com.citycar.model.message.ProductFilter;
import com.citycar.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/product")
public class ProductWebService {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    private static final Logger logger = LoggerFactory.getLogger(CategoryWebService.class);
    Map<String, Object> map = new HashMap<>();


    @GetMapping()
    public ResponseEntity<Map<String, Object>> getAllProduct(
            @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(value = "limit", required = false, defaultValue = "0") Integer limit,
            @RequestParam(value = "id", required = false, defaultValue = "0") Integer id,
            @RequestParam(value = "name", required = false, defaultValue = "") String name,
            @RequestParam(value = "categoryId", required = false, defaultValue = "0") Integer categoryId,
            @RequestParam(value = "userId", required = false, defaultValue = "0") Integer userId,
            @RequestParam(value = "wishlistUserId", required = false, defaultValue = "0") Integer wishlistUserId
    ) {
        try {
            Pagination pagination=new Pagination();
            pagination.setLimit(limit);
            pagination.setPage(page);
            ProductFilter productFilter=new ProductFilter(id,name,categoryId,userId,wishlistUserId);
            System.out.println(productFilter);
            List<Product> productList = productService.getAllProduct(pagination,productFilter);
            System.out.println(productList);
            if (productList.size()>0) {
                pagination.setTotalCount(productList.get(0).getTotalCount());
                if (page != null && limit != null && page > 0 && limit > 0) {
                    map.put(JavaUtil.STRING_PAGENATION, pagination);
                }
            }
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, productList);
            return new ResponseEntity<>(map, HttpStatus.OK);

        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }
}
