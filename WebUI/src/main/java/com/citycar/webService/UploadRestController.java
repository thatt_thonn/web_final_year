package com.citycar.webService;

import com.citycar.model.message.JavaUtil;
import com.citycar.service.ProductImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
public class UploadRestController {

    @Value("${spring.upload.server.path}")
    String serverPath;

    @PostMapping("/api/upload")
    public ResponseEntity<Map<String, Object>> uploadFile(@RequestParam("file") MultipartFile file) {

        System.out.println("Loading imag upload....");
        Map<String, Object> map = new HashMap<>();
        File path = new File(serverPath);
        System.out.println("file: " + file);
        if (!path.exists()) {
            if (path.mkdirs()) {

            } else {
            }
        }
        try {

            String caption = "www.web-ecommerce.com";


            String filename = file.getOriginalFilename();
            String extension = filename.substring(filename.lastIndexOf('.') + 1);
            filename = UUID.randomUUID() + "." + extension;
//            Files.copy(file.getInputStream(), Paths.get(serverPath, filename));
            JavaUtil.addTextWatermark(caption, extension, file, new File(serverPath + filename));
            System.out.println("file001: " + file);
            String url = "/image/" + filename;
            map.put("data", url);
            map.put("filename", filename);
            map.put("extension", extension);
            map.put("path", path);
            map.put("status", true);

        } catch (Exception e) {
            map.put("size", "cannot upload");
            map.put("status", false);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }



    private ProductImageService productImageService;

    @Autowired
    public void setProductImageService(ProductImageService productImageService) {
        this.productImageService = productImageService;
    }

    // remove from database and directory server
    @PostMapping("/api/remove")
    public ResponseEntity<Map<String, Object>> removeFile(@RequestParam("url") String url, @RequestParam("id") Integer id) {
        Map<String, Object> map = new HashMap<>();
        try {
            Integer deleteId = productImageService.deleteProductImageById(id);
            if (deleteId > 0) {
                if (url != null) {
                    File removeFile = new File(url);
                    String strUrl = removeFile.getName();
                    File deleteImage = new File(serverPath + "\\" + strUrl);
                    deleteImage.delete();
                    map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
                    map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
                    return new ResponseEntity<>(map, HttpStatus.OK);
                }
            }

        } catch (Exception e) {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
        map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
        map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
        return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
    }

}
