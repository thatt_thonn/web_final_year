package com.citycar.webService;

import com.citycar.model.InvoiceDetail;
import com.citycar.model.User;
import com.citycar.model.message.JavaUtil;
import com.citycar.repository.MyCardRepository;
import com.citycar.service.MyCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/card")
public class MyCardWebService {
    Map<String, Object> map = new HashMap<>();

    private MyCardService myCardService;

    @Autowired
    public void setMyCardService(MyCardService myCardService) {
        this.myCardService = myCardService;
    }

    @GetMapping("/{userId}")
    public ResponseEntity<Map<String, Object>> getAllCard(@PathVariable("userId")Integer userId) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, myCardService.getAllCard(userId));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/add-card/{productId}/{amount}")
    public ResponseEntity<Map<String, Object>> addToCard(@PathVariable("productId")Integer productId,@PathVariable("amount")Float amount) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            InvoiceDetail invoiceDetail=new InvoiceDetail();
            invoiceDetail.setAmount(amount);
            invoiceDetail.setStatus(false);
            invoiceDetail.setQty(1);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principle = authentication.getPrincipal();
            Integer userId=((User) principle).getId();
            invoiceDetail.setUserId(userId);
            invoiceDetail.setProductId(productId);
            map.put(JavaUtil.STRING_REC, myCardService.addToCard(invoiceDetail));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }



    private MyCardRepository myCardRepository;


    @Autowired
    public void setMyCardRepository(MyCardRepository myCardRepository) {
        this.myCardRepository = myCardRepository;
    }

    @GetMapping("/total-price-card")
    public ResponseEntity<Map<String, Object>> getTotalCount() {
        try {

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principle = authentication.getPrincipal();
            Integer userId=((User) principle).getId();
            Float totalPriceCard=myCardService.totalPriceCard(userId);
            if(totalPriceCard==null){
                totalPriceCard=0.00f;
            }

            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);

            map.put(JavaUtil.STRING_REC, totalPriceCard);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/count")
    public ResponseEntity<Map<String, Object>> getTotalCountData() {
        try {

            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principle = authentication.getPrincipal();
            Integer userId=((User) principle).getId();
            Integer totalPriceCard=myCardRepository.totalData(userId);
            if(totalPriceCard==null){
                totalPriceCard=0;
            }

            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);

            map.put(JavaUtil.STRING_REC, totalPriceCard);
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }
}
