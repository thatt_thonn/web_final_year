package com.citycar.webService;

import com.citycar.model.message.JavaUtil;
import com.citycar.service.InvoiceService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/invoice")
public class InvoiceWebService {

    private InvoiceService invoiceService;

    @Autowired
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }
    Map<String, Object> map = new HashMap<>();


    @GetMapping()
    public ResponseEntity<Map<String, Object>> getInvoiceBuyUserId(
            @RequestParam(value = "id", required = false, defaultValue = "0") Integer id
    ) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, invoiceService.getInvoiceByUser(id));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }
}
