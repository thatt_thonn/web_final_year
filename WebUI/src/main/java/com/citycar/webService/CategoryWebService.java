package com.citycar.webService;

import com.citycar.model.message.JavaUtil;
import com.citycar.model.Category;
import com.citycar.service.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/category")
public class CategoryWebService {

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    private static final Logger logger = LoggerFactory.getLogger(CategoryWebService.class);
    Map<String, Object> map = new HashMap<>();


    @GetMapping()
    public ResponseEntity<Map<String, Object>> getAllCategory() {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, categoryService.getAllCategory());
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String, Object>> getCategoryById(@PathVariable("id")Integer id) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, categoryService.getCategoryById(id));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }


    /*{"name": "Coke", "userId": "3"}*/
    @PostMapping()
    public ResponseEntity<Map<String, Object>> createCategory(@RequestBody Category category) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, categoryService.createCategory(category));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }


   /* {
        "id": "14",
        "name": "Sprite",
        "userId": "3"
    }*/
    @PutMapping()
    public ResponseEntity<Map<String, Object>> updateCategory(@RequestBody Category category) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, categoryService.updateCategory(category));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Object>> deleteCategory(@PathVariable("id") Integer id) {
        try {
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            map.put(JavaUtil.STRING_REC, categoryService.deleteCategory(id));
            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }




}
