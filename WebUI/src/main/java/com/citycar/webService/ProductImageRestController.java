package com.citycar.webService;

import com.citycar.model.message.ImagesFilter;
import com.citycar.model.message.JavaUtil;
import com.citycar.service.ProductImageService;
import com.citycar.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProductImageRestController {

    @Value("${spring.upload.server.path}")
    String serverPath;

    private ProductImageService productImageService;

    @Autowired
    public void setProductImageService(ProductImageService productImageService) {
        this.productImageService = productImageService;
    }

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("/api/image")
    public ResponseEntity<Map<String, Object>> saveTempImage(@RequestBody ImagesFilter imagesFilter) {
        Map<String, Object> result = new HashMap<>();
        try {
            if (productImageService.saveTempImage(imagesFilter) > 0) {
                result.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
                result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
                return new ResponseEntity<>(result, HttpStatus.OK);
            } else {
                result.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
                result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
                return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            e.printStackTrace();
            result.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/api/image")
    public ResponseEntity<Map<String, Object>> saveTempImage(@RequestParam("uuid") String uuid) {
        Map<String, Object> result = new HashMap<>();
        try {
            String str = productImageService.removeTempImage(URLDecoder.decode(uuid));
            if (str != null) {
                File removeFile = new File(str);
                String strUrl = removeFile.getName();
                File deleteImage = new File(serverPath + "\\" + strUrl);
                deleteImage.delete();
            }
            result.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            result.put(JavaUtil.STRING_REC, str);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            result.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

    }

    @DeleteMapping("/api/image/all")
    public ResponseEntity<Map<String, Object>> deleteAllTempImage(@RequestParam("userId") Integer userId) {
        System.out.println("/user/api/image/all: "+userId);
        Map<String, Object> result = new HashMap<>();
        try {
            List<ImagesFilter> imagesFilters = productImageService.getAllTempImageByUserId(userId);
            if (imagesFilters != null) {
                for (ImagesFilter img : imagesFilters
                        ) {
                    File removeFile = new File(img.getFilename());
                    String strUrl = removeFile.getName();
                    File deleteImage = new File(serverPath + "\\" + strUrl);
                    deleteImage.delete();
                    productImageService.removeTempImage(img.getUuid());
                }
            }
            result.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            result.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            result.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }
    }


}
