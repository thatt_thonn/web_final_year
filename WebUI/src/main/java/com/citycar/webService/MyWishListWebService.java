package com.citycar.webService;


import com.citycar.model.User;
import com.citycar.model.WishList;
import com.citycar.model.message.JavaUtil;
import com.citycar.service.MyWishlistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/api/wishlist")
public class MyWishListWebService {

    private MyWishlistService myWishlistService;

    @Autowired
    public void setMyWishlistService(MyWishlistService myWishlistService) {
        this.myWishlistService = myWishlistService;
    }


    Map<String, Object> map = new HashMap<>();
    Authentication authentication = null;

    @GetMapping("/{productId}")
    public ResponseEntity<Map<String, Object>> getCategoryById(@PathVariable("productId")Integer productId) {
        try {
            authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principle = authentication.getPrincipal();
            Integer authUserId = ((User) principle).getId();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
            WishList wishList=new WishList();
            wishList.setProductId(productId);
            wishList.setUserId(authUserId);
//            Integer status=myWishlistService.createWishlist(wishList);
//            if(status==0){
//                // action not suucess
//            }else if(status==1){
//                //remove success
//            }else if(status==2){
//                // insert accest
//            }
            map.put(JavaUtil.STRING_REC, myWishlistService.createWishlist(wishList));

            return new ResponseEntity<>(map, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
            map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
            return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
        }
    }

//    countAllWishListUser
@GetMapping("/count")
public ResponseEntity<Map<String, Object>> countAllWishListUser() {
    try {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principle = authentication.getPrincipal();
        Integer authUserId = ((User) principle).getId();
        map.put(JavaUtil.STRING_STATUS, JavaUtil.STATUS_SUCCESS);
        map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_SUCCESS);
        map.put(JavaUtil.STRING_REC, myWishlistService.countAllWishListUser(authUserId));
        return new ResponseEntity<>(map, HttpStatus.OK);
    } catch (Exception e) {
        e.printStackTrace();
        map.put(JavaUtil.STRING_STATUS, JavaUtil.TATUS_UN_SUCCESS);
        map.put(JavaUtil.STRING_MESSAGE, JavaUtil.MESSAGE_UN_SUCCESS);
        return new ResponseEntity<>(map, HttpStatus.NOT_FOUND);
    }
}

}
