package com.citycar.controller;

import com.citycar.model.Invoice;
import com.citycar.model.InvoiceDetail;
import com.citycar.model.User;
import com.citycar.repository.MyCardRepository;
import com.citycar.service.InvoiceService;
import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MyCardController {

    private InvoiceService invoiceService;

    @Autowired
    public void setInvoiceService(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    private MyCardRepository myCardRepository;

    @Autowired
    public void setMyCardRepository(MyCardRepository myCardRepository) {
        this.myCardRepository = myCardRepository;
    }

    @GetMapping("/user/card")
    private String card(Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principle = authentication.getPrincipal();
        Integer userId=((User) principle).getId();
        model.addAttribute("card",myCardRepository.getAllCard(userId));
        Float totalPriceCard=myCardRepository.totalPriceCard(userId);
        if(totalPriceCard==null){
            totalPriceCard=0.00f;
        }
        model.addAttribute("totalAmount",totalPriceCard);
        System.out.println("myCardRepository.totalPriceCard(userId): "+totalPriceCard);
        return "client/card";
    }

}
