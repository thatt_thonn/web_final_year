package com.citycar.controller;

import com.citycar.model.User;
import com.citycar.service.RoleService;
import com.citycar.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RegisterController {


    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    public void setUserDetailsService(UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    private RoleService roleService;

    @Autowired
    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping("/register")
    private String register(){
        return "client/register";
    }

    @PostMapping("/register")
    private String store(@ModelAttribute User user,HttpServletRequest request){

        try{

            Integer roleId= roleService.getRoleByName("USER");
            user.setRoleId(roleId);
            user.setFirstName(user.getFirstName().toUpperCase());
            user.setLastName(user.getLastName().toUpperCase());
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
            if(userDetailsService.saveUser(user)>0){
                // load user info
                UserDetails users = userDetailsService.loadUserByUsername(user.getEmail());
                Authentication auth = null;

                auth = new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(auth);
                String previousURL = (String) request.getSession().getAttribute("REDIRECT_URL");

                if (previousURL == null) {
                    previousURL = "/";
                }
                return "redirect:" + previousURL;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "client/register";
    }
}
