package com.citycar.controller;

import com.citycar.model.Category;
import com.citycar.model.Product;
import com.citycar.model.message.Pagination;
import com.citycar.model.message.ProductFilter;
import com.citycar.service.CategoryService;
import com.citycar.service.ProductService;
import com.citycar.webService.CategoryWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;


@Controller
public class ProductController {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

    @GetMapping("/product/{id}")
    private String product(@PathVariable("id") Integer id, Model model) {
        Product product = null;
        List<Product> products = null;
        try {
            productService.updateTotalViewProduct(id);
            products = productService.getAllProduct(new Pagination(), new ProductFilter(id, "", 0, 0, 0));
            if (products != null) {
                product = products.get(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        model.addAttribute("product", product);
        logger.info("Product View: " + product);
        return "client/product";
    }


    @GetMapping("/product/remove/{id}")
    private String deleteProduct(@PathVariable("id") Integer id) {
        try {
            productService.deleteProductById(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/admin/dashboard/";
    }




    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/category/{id}")
    private String productCate(@PathVariable("id") Integer id,Model model){
        model.addAttribute("category_id",id);
        model.addAttribute("catename","Category : "+categoryService.getCategoryById(id).getName());
        return "client/category";
    }

}
