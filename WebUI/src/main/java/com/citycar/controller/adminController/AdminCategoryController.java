package com.citycar.controller.adminController;

import com.citycar.model.Category;
import com.citycar.model.Product;
import com.citycar.model.User;
import com.citycar.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AdminCategoryController {
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/admin/dashboard/all_category")
    public String index(Model model) {
        try {
            model.addAttribute("category", categoryService.getAllCategory());
        } catch (Exception e) {
            model.addAttribute("category", null);
        }
        return "admin/category_list";
    }


    @GetMapping("/admin/dashboard/create_category")
    public String createCategory(Model model) {
        model.addAttribute("category", new Category());
        return "admin/create_category";
    }


    Authentication authentication = null;

    @PostMapping("/admin/dashboard/create_category")
    public String createCategory(@ModelAttribute Category category) {
        try {
            authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principle = authentication.getPrincipal();
            Integer authUserId = ((User) principle).getId();
            category.setUserId(authUserId);
            System.out.println(categoryService.createCategory(category));;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/admin/dashboard/all_category";
    }
}
