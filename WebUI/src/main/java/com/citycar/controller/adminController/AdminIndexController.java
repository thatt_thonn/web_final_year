package com.citycar.controller.adminController;


import com.citycar.model.Product;
import com.citycar.model.ProductImage;
import com.citycar.model.User;
import com.citycar.model.message.ImagesFilter;
import com.citycar.service.CategoryService;
import com.citycar.service.ProductImageService;
import com.citycar.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/admin/dashboard")
public class AdminIndexController {


    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("product", productService.getAllProductAdmin());
        System.out.println(productService.getAllProductAdmin());
        return "admin/index";
    }


    @GetMapping("/test")
    @ResponseBody
    public List<Product> test() {
        return productService.getAllProductAdmin();
    }

    Authentication authentication = null;

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/create_product")
    public String createProduct(Model model) {
        try {
            authentication = SecurityContextHolder.getContext().getAuthentication();
            Object principle = authentication.getPrincipal();
            Integer authUserId = ((User) principle).getId();
            productImageService.deleteAllTempImage(authUserId);
            model.addAttribute("category", categoryService.getAllCategory());
        } catch (Exception e) {
            model.addAttribute("category", null);
        }
        model.addAttribute("product",new Product());
        return "admin/f_create_product";
    }




    private ProductImageService productImageService;

    @Autowired
    public void setProductImageService(ProductImageService productImageService) {
        this.productImageService = productImageService;
    }

    @PostMapping("/product/save")
    public String saveProduct(@ModelAttribute Product product){

        System.out.println(product.toString());
        String userId = null;
        Integer priorityNo = 0;
        Integer count = 0;
        String redirect = "redirect:/admin/dashboard/";


        try {
            authentication = SecurityContextHolder.getContext().getAuthentication();

            if (!authentication.getPrincipal().equals("anonymousUser")) {
                Object principle = authentication.getPrincipal();
                if (authentication.getAuthorities().equals("ROLE_ADMIN")) {
                    redirect = "redirect:/admin/dashboard/";
                }
                //save product
                Integer authUserId = ((User) principle).getId();


                // set null value
                product.setStatus(true);
                product.setTotalView(0);
                User user = new User();
                user.setId(authUserId);
                product.setUser(user);

                Integer productId = productService.saveProduct(product);
                int i = 0;
                if (productId > 0) {
                    List<ImagesFilter> imagesFilters = productImageService.getAllTempImageByUserId(authUserId);
                    ProductImage productImage = null;
                    if (imagesFilters != null) {
                        for (ImagesFilter img : imagesFilters
                                ) {
                            if (i == 0) {
                                productImage = new ProductImage(img.getFilename(), 1, productId);
                            } else {
                                productImage = new ProductImage(img.getFilename(), 0, productId);
                            }
                            i++;
                            Integer saveProductImageNote = productImageService.saveProductImage(productImage);
                            if (saveProductImageNote > 0) {
                            }
                        }
                        productImageService.deleteAllTempImage(authUserId);
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return redirect;
    }
}
