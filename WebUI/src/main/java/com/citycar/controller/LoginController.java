package com.citycar.controller;

import com.citycar.webService.CategoryWebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {


    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @GetMapping("/login")
    private String login(){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        logger.info(auth.getPrincipal().toString());
        if (!auth.getPrincipal().equals("anonymousUser")) {
            return "redirect:/";
        }
        return "client/login";
    }
}
