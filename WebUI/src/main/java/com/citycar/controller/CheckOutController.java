package com.citycar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CheckOutController {

    @GetMapping("/checkout")
    private String checkout(){
        return "client/checkout";
    }

}
