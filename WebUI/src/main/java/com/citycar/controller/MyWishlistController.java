package com.citycar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MyWishlistController {
    @GetMapping("/user/wishlist")
    private String wishList(){
        return "client/wish_list";
    }
}
