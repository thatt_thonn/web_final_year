package com.citycar.controller;

import com.citycar.service.CategoryService;
import com.citycar.webService.CategoryWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StoreController {

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping("/store")
    private String store(Model model) {
        try {
            model.addAttribute("category", categoryService.getAllCategory());
        } catch (Exception e) {
            model.addAttribute("category", null);
        }
        return "client/store";
    }

}
