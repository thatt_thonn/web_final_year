package com.citycar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TestingController {


    @GetMapping("/blank")
    private String blank(){
        return "client/blank";
    }

}
