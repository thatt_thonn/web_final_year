package com.citycar.config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Component
public class CustomSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        String redirectUrl = (String) httpServletRequest.getSession().getAttribute("REDIRECT_URL");
        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
            System.out.println("SUCCESS: " + grantedAuthority.getAuthority());
            if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
                redirectUrl = "/admin/dashboard/";
            } else if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
                redirectUrl = "/";
            }
        }
        httpServletResponse.sendRedirect(redirectUrl);
    }
}
