----- ******** Product Sql ********--------
-- INSERT
INSERT INTO public.e_product
(create_date, description, expire_advertise, expire_date, modified_date, "name", price, qty, status, category_id, users_id, total_view)
VALUES(now(), '', now()+interval '3 day', now()+interval '3 month', now(), #{name}, #{price}, #{qty}, true, #{category_id}, #{user_id}, #{total_view});

-- UPDATE
UPDATE public.e_product
SET modified_date=now(), "name"=#{name}, price=#{price}, qty=#{qty}, category_id=#{category_id}, users_id=#{user_id}
WHERE id=nextval('e_product_id_seq'::regclass);

-- DELETE
DELETE FROM public.e_product
WHERE id=nextval('e_product_id_seq'::regclass);

-- SELECT
SELECT 
(count(pr.id) over(PARTITION BY pr.status)) as totalCount,
pr.id, 
pr.create_date, 
pr.description, 
pr.expire_advertise, 
pr.expire_date, 
pr.modified_date, 
pr.name, 
pr.price, 
pr.qty, 
pr.status, 
pr.category_id, 
pr.users_id, 
pr.total_view,
pr.expire_date<CURRENT_TIMESTAMP as isExpire,
(CASE WHEN pr.expire_advertise>NOW() THEN TRUE ELSE FALSE END) AS isAdvertise,
(SELECT pi.url FROM e_product_image pi WHERE pi.product_id=pr.id LIMIT 1)
FROM public.e_product pr




----- ******** Category Sql ********--------
-- SELECT
SELECT 
ca.id, 
ca.name, 
ca.created_date, 
.modified_date, 
ca.user_id,
(select count(*) from e_product where category_id=ca.id) as total_product
FROM public.e_category ca;

-- INSERT
INSERT INTO public.e_category
(name, created_date, modified_date, user_id)
VALUES(#{name}, now(), now(), #{user_id}) returning id,name,modified_date,created_date,user_id ;

-- UPDATE
UPDATE public.e_category
SET name=#{name}, modified_date=now(), user_id=#{userId}
WHERE id=#{id};

-- DELETE
DELETE FROM public.e_category
WHERE id=#{id};






----- ******** User Sql ********--------
-- INSERT
INSERT INTO public.e_user
(first_name, last_name, email, phone, "password", gender, 
status, created_date, modified_date, role_id, profile)
VALUES(#{first_name}, #{last_name}, #{email}, #{phone}, #{password}, 'M', true, now(), now(), #{role_id}, #{profile});


-- UPDATE
UPDATE public.e_user
SET first_name=#{first_name}, last_name=#{last_name}, email=#{email}, phone=#{phone}, "password"=#{password}, gender=#{gender}, status=true, created_date=now(), modified_date=now(), role_id=#{role_id}, profile=#{profile}
WHERE id=nextval('e_user_id_seq'::regclass);

-- SELECT
SELECT 
id, 
first_name, 
last_name, 
email, 
phone, 
"password", 
gender, 
status, 
created_date, 
modified_date, 
role_id, 
profile
FROM public.e_user;

-- DELETE
DELETE FROM public.e_user
WHERE id=nextval('e_user_id_seq'::regclass);




----- ******** User Sql ********--------
-- SELECT
SELECT id, "name", description, created_date, modified_date
FROM public.e_role;

-- INSERT
INSERT INTO public.e_role
("name", description, created_date, modified_date)
VALUES(#{name}, #{description}, now(), now());

-- DELETE
DELETE FROM public.e_role
WHERE id=nextval('e_role_id_seq'::regclass);

-- UPDATE
UPDATE public.e_role
SET "name"=#{name}, description=#{description}, modified_date=now()
WHERE id=nextval('e_role_id_seq'::regclass);



----- ******** Wishlist Sql ********--------
-- INSERT
INSERT INTO public.e_whish_list(product_id, user_id)VALUES(#{product_id}, #{user_id});

-- DELETE
DELETE FROM public.e_whish_list
WHERE id=nextval('e_whish_list_id_seq'::regclass);

-- SELECT
SELECT id, product_id, created_date, modified_date, user_id
FROM public.e_whish_list;

-- UPDATE
UPDATE public.e_whish_list
SET product_id=#{product_id},  modified_date=now(), user_id=#{user_id}
WHERE id=nextval('e_whish_list_id_seq'::regclass);



-- e_product_image sql ---
INSERT INTO public.e_product_image
("name", url, created_date, modified_date, product_id)
VALUES(#{name}, #{url}, now(), now(), #{product_id});




--- e_invoice sql ----
-- SELECT
SELECT id, "date", user_id, payment_id, total_amount, status, created_date, modified_date
FROM public.e_invoice;

-- DELETE
DELETE FROM public.e_invoice
WHERE id=nextval('e_invoice_id_seq'::regclass);



--- e_invoice_detail sql ---
-- SELECT
SELECT id, "date", product_id, created_date, modified_date, amount, invoice_id, status
FROM public.e_invoice_detail;


--- e_payment sql ---
SELECT id, payment_type, auth_user_id, auth_datetime, "desc", status
FROM public.e_payment;





























