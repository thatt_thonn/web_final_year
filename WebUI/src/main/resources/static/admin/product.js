var special = {};

var myCurrentPage = 1;
var check = true;
var limit = 16;
var str_role = 'ADMIN';
var loId = '';
var brId = '';
var key_srch = '';
var v_type=2;
var check=true;


$(document).ready(function () {

    $("#num-2").click(function () {
        // alert("Number 2");
        v_type = 2;
        special.allProduct(key_srch, brId, loId, myCurrentPage, limit);
    })

    $("#num-4").click(function () {
        // alert("Number 4");
        v_type = 4;
        special.allProduct(key_srch, brId, loId, myCurrentPage, limit);
    })


    /*search function*/
    $('#btn_srch').click(function () {
        loId = $('.LO_LIST_OUTPUT').val();
        brId = $('.BR_LIST_OUTPUT').val();
        key_srch = $('#input_srch').val();
        console.clear();
        if (brId == 0)
            brId = '';
        if (loId == 0)
            loId = '';
        myCurrentPage=1;
        special.allProduct(key_srch, brId, loId, myCurrentPage, limit);
    });


    //list combo box
    special.listAllBranch = function () {
        $.ajax({
            url: "/api/branch/all",
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                if (data.STATUS == "0000") {
                    var d =
                        {
                            "id": 0,
                            "name": "Default",
                            "photo": "http://citycar999.com/assets/PNG/Volkswagen.png",
                            "description": null,
                            "created_date": "2019-05-04",
                            "modified_date": "2019-05-04",
                            "total_products": 0
                        };
                    var template = $.templates("#RENDER_LIST_BR");
                    data.REC.unshift(d);
                    var htmlOutput = template.render(data.REC);
                    $(".BR_LIST_OUTPUT").html(htmlOutput);
                }
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });
    }

    special.listAllBranch();


    special.listAllLocation = function () {
        $.ajax({
            url: "/api/location",
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                if (data.STATUS == "0000") {
                    var d =
                        {
                            "id": 0,
                            "name": "Kompong Chhnang",
                            "description": null,
                            "created_date": "2019-05-04",
                            "modified_date": "2019-05-04",
                            "total_products": 3
                        };
                    var template = $.templates("#RENDER_LIST_LO");
                    data.REC.unshift(d);
                    var htmlOutput = template.render(data.REC);
                    $(".LO_LIST_OUTPUT").html(htmlOutput);
                }
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });
    }

    special.listAllLocation();


    /*** Function load ***/

    special.allProduct = function (name, brId, loId, page, limit) {
        // $('.loading').show();
        $(".PRODUCT_ID_RENDER").empty();
        $(".PRODUCT_ID_RENDER").append(img_loading);
        $.ajax({
            url: "/api/product/all?status=1&limit=" + limit + "&page=" + page + "&role=" + str_role + "&brId=" + brId + "&loId=" + loId+"&name="+name,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                // $('.loading').hide();
                // console.clear();
                // console.log("/api/product/all?limit=" + limit + "&page=" + page + "&role=" + str_role + "&brId=" + brId + "&loId=" + loId+"&name="+name);
                // console.log("data",data);
                if (data.STATUS == "0000") {
                    if(v_type==2){
                        var template = $.templates("#RENDER_PRODUCT");
                        var htmlOutput = template.render(data.REC);
                        $(".PRODUCT_ID_RENDER").html(htmlOutput);
                    }
                    else{
                        var template = $.templates("#RENDER_PRODUCT-4");
                        var htmlOutput = template.render(data.REC);
                        $(".PRODUCT_ID_RENDER").html(htmlOutput);
                    }
                    if(check){
                        special.setPagination(data.PAGENATION.total_pages, page);
                    }
                }
                else if (data.STATUS == "1111") {
                    var message = {msg: name}
                    var template = $.templates("#RENDER_ERR_SRCH");
                    var htmlOutput = template.render(message);
                    $(".PRODUCT_ID_RENDER").html(htmlOutput);
                    $('#pagination-here').empty();
                }
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });

    };


    /** Pagination **/
    special.setPagination = function (totalPage, currentPage) {
        if (totalPage == 1) {
            $('#pagination-here').empty();
        } else {
            $('#pagination-here').empty();
            $('#pagination-here').bootpag({
                total: totalPage,
                page: currentPage,
                maxVisible: 6,
                leaps: true,
                firstLastUse: true,
                first: 'First',
                last: 'Last',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            }).on("page", function (event, currentPage) {
                check=false;
                myCurrentPage = currentPage;
                special.allProduct(key_srch,brId,loId,currentPage, limit);
                $('html, body').animate({
                    scrollTop: $("#PAGE_SCROLL").offset().top
                }, 300);
            });
        }
    };


    /** OnLoad **/

    special.allProduct(key_srch, brId, loId, myCurrentPage, limit);


});