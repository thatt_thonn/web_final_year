
//Model object
function Category(id,name,nameKh,description,haveDetail,createDate,modifiedDate,photo,totalProducts){
    this.id = id;
    this.name = name;
    this.nameKh = nameKh;
    this.description = description;
    this.haveDetail = haveDetail;
    this.createDate = createDate;
    this.modifiedDate = modifiedDate;
    this.photo = photo;
    this.totalProducts = totalProducts;
}

function Branch(id,name,description,createDate,modifiedDate,photo,image,totalProducts,totalRecords){
    this.id = id;
    this.name = name;
    this.description = description;
    this.createDate = createDate;
    this.modifiedDate = modifiedDate;
    this.photo = photo;
    this.image = image;
    this.totalProducts = totalProducts;
    this.totalRecords = totalRecords;
} 

function Product(){
    this.advertise= ko.observable();
    this.condition= ko.observable();
    this.contact_no= ko.observable();
    this.contact_name = ko.observable();
    this.contact_desc = ko.observable();
    this.created_date= ko.observable();
    this.description= ko.observable();
    this.disable_by= ko.observable();
    this.expire_advertise= ko.observable();
    this.expire_date= ko.observable();
    this.email = ko.observable();
    this.id= ko.observable();
    this.is_advertise= ko.observable();
    this.location= ko.observable();
    this.modified_date= ko.observable();
    this.name= ko.observable();
    this.price= ko.observable();
    this.product_images= ko.observable();
    this.qty= ko.observable();
    this.status= ko.observable();
    this.total_count= ko.observable();
    this.total_view= ko.observable();
    this.type_tax= ko.observable();
    this.url= ko.observable();
    this.user= ko.observable();
    this.year = ko.observable();

    this.category = ko.observable();
    this.branch = ko.observable();
}

function Pagination(){
    var self = this;

    self.pagin = ko.observableArray([]);

    self.setPagin = function(pageToShow,currentPage,totalPage){
        var halfPage = (pageToShow / 2) + 1;
        if (currentPage<halfPage){
            start = 1;
        }
        else if (currentPage >= totalPage-(halfPage-1)){
            start = (totalPage - pageToShow) + 1;
        }
        else if(currentPage>halfPage){
            start = currentPage - halfPage+1;
        }
        for (i = start; i <= (start+pageToShow-1); i++){
            self.pagin.push(i);
        }
    }
}

//VIew model object
function ProductViewModel(){
    var self = this;
    self.url = "http://localhost:1111/"
    self.listProducts = ko.observableArray([]);
    self.pagin = ko.observableArray([]);
    self.viewPage = ko.observable(1);

    self.setPagin = function(pageToShow,currentPage,totalPage){
        var halfPage = Math.ceil(pageToShow / 2);
        if (currentPage<halfPage){
            start = 1;
        }
        else if (currentPage >= totalPage-(halfPage-1)){
            start = (totalPage - pageToShow) + 1;
        }
        else if(currentPage>halfPage){
            start = currentPage - halfPage+1;
        }
        for (i = start; i <= (start+pageToShow-1); i++){
            self.pagin.push(i);
        }
    }

    self.getAllProduct = function(limit,page){
        self.viewPage(page);
         $.ajax({
                    url: "/api/product/all?limit="+limit+"&page="+page,
                    type: "GET",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function (data) {
                        var paginObj = new Pagination();
                        self.listProducts.removeAll();
                        self.pagin.removeAll();
                        if (data.STATUS == "0000") {
                            if(data.REC.length>0){
                                self.setPagin(5,page,data.PAGENATION.total_pages);
                                for(i=0;i<data.REC.length;i++){
                                    self.listProducts.push(
                                        new Product()
                                        .id(data.REC[i].id)
                                        .name(data.REC[i].name)
                                        .qty(data.REC[i].qty)
                                        .price(data.REC[i].price)
                                        .status(data.REC[i].status)
                                        .condition(data.REC[i].condition)
                                        .year(data.REC[i].year)
                                        .url(data.REC[i].url)
                                        .email(data.REC[i].email)
                                        .description(data.REC[i].description)
                                        .category(new Category(
                                            data.REC[i].category.id,
                                            data.REC[i].category.name,
                                            data.REC[i].category.name_kh,
                                            data.REC[i].category.description,
                                            data.REC[i].category.have_detail,
                                            data.REC[i].category.created_date,
                                            data.REC[i].category.modified_date,
                                            data.REC[i].category.photo,
                                            data.REC[i].category.total_products
                                            )
                                        ).location(data.REC[i].location)
                                        .branch(new Branch(
                                            data.REC[i].branch.id,
                                            data.REC[i].branch.name,
                                            data.REC[i].branch.description,
                                            data.REC[i].branch.created_date,
                                            data.REC[i].branch.modified_date,
                                            data.REC[i].branch.photo,
                                            data.REC[i].branch.image,
                                            data.REC[i].branch.total_products,
                                            data.REC[i].branch.total_rec
                                        )).user(data.REC[i].user)
                                        .advertise(data.REC[i].advertise)
                                        .expire_advertise(data.REC[i].expire_advertise)
                                        .type_tax(data.REC[i].type_tax)
                                        .contact_no(data.REC[i].contact_no)
                                        .created_date(data.REC[i].created_date)
                                        .modified_date(data.REC[i].modified_date)
                                        .disable_by(data.REC[i].disable_by)
                                        .total_view(data.REC[i].total_view)
                                        .is_advertise(data.REC[i].is_advertise)
                                        .total_count(data.REC[i].total_count)
                                        .contact_name(data.REC[i].contact_name)
                                        .product_images(data.REC[i].product_images)
                                    );
                                }
                            }
                        }
                    },
                    error: function (data, status, er) {
                        console.log(data);
                    }
                });
    }
}

// using real object
var vmProduct = new ProductViewModel();
vmProduct.getAllProduct(5,1);
console.log(vmProduct.viewPage())
ko.applyBindings(vmProduct);

$(function(){
    $('#pagination').on('click', 'li', function() {
        vmProduct.getAllProduct(5,$(this).text());
    });
});
