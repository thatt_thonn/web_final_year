$(function () {
    $('body').append('<div id="toTop" class="btn btn-info" style="float: right;" ><i class="fa fa-arrow-up" style="color:white;"></i></div>');
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
    $('#toTop').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    });



    // load static function
    loadCategory();
});





function loadCategory() {
    $.ajax({
        url: "/api/category",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            var first_item=  ' <option value="0">All Categories</option>';
            if (data.STATUS == "0000") {
                data.REC.forEach(function (d) {
                    first_item+='<option value="'+d.id+'">'+d.name+'('+d.total_product+')</option>';
                });
                $('#select_category').html(first_item);
            }
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}