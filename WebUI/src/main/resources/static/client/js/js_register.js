function checkPassword(pw, confPass) {
    var passw = pw.length;
    if (passw >= 8 && passw <= 16) {
        // true password is right
        if (confPass != pw) {
            // clear data from password and confirm password
            return false;
        }
        return true;
    }
    else {
        return false;
    }
}


$(function () {

    $('#error_first_name').hide();
    $('#error_last_name').hide();
    $('#error_phone').hide();
    $('#error_email').hide();
    $('#error_password').hide();


    $('#btn-register').click(function () {
        if ($.trim($('#firstName').val()) == '') {
            $('#error_first_name').show()
            $('#firstName').focus();
            return;
        }

        if ($.trim($('#lastName').val()) == '') {
            $('#error_last_name').show();
            $('#lastName').focus()
            return;
        }

        if ($.trim($('#phone').val()) == '') {
            $('#error_phone').show();
            $('#phone').focus();
            return;
        }

        if ($.trim($('#email').val()) == '') {
            $('#error_email').show();
            $('#email').focus();
            return;
        }

        if ($.trim($('#password').val()) == '') {
            $('#error_password').show();
            $('#password').focus();
            return;
        }
        if (checkPassword($.trim($('#password').val()), $.trim($('#confirm_password').val())) == false) {
            $('#error_password').show();
            $('#password').val('');
            $('#confirm_password').val('');
            $('#password').focus();
            return;
        }

        $('#submit-data').submit();
    });

});


