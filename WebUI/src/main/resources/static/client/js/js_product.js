var index = {};

var myCurrentPage = 1;
var limit = 4;
var check = true;


$(document).ready(function () {

    if(authen_status==true){
        countWishlist();
        countMyCard();
    }

    /*** Function ***/
    index.allProduct = function () {
        $.ajax({
            url: "/api/product?categoryId=0&id=0&limit=8&page=0&userId=0&wishlistUserId=0",
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                var htmlOutput = '';
                data.REC.forEach(function (d) {
                    htmlOutput += '<div class="col-md-3 col-xs-6">\n' +
                        '                   <a href="/product/'+d.id+'">         <div class="product">\n' +
                        '                                <div class="product-img">\n' +
                        '                                    <img src="' + d.url + '" alt="">\n' +
                        '                                    <div class="product-label">\n' ;

                    if(d.is_advertise==true){
                        htmlOutput+=    '     <span class="new">NEW</span>\n';
                    }

                    htmlOutput+=
                        '                                    </div>\n' +
                        '                                </div></a>\n' +
                        '                                <div class="product-body">\n' +
                        '                                    <p class="product-category">' + d.category.name + '</p>\n' +
                        '                                    <h3 class="product-name"><a href="/product/'+d.id+'">' + d.name + '</a></h3>\n' +
                        '                                    <h4 class="product-price">$' + d.price + '\n' +
                        '\n' +
                        '                                    </h4>\n' +
                        '                                    <div class="product-rating">\n' +
                        '                                        <i class="fa fa-star"></i>\n' +
                        '                                        <i class="fa fa-star"></i>\n' +
                        '                                        <i class="fa fa-star"></i>\n' +
                        '                                        <i class="fa fa-star"></i>\n' +
                        '                                        <i class="fa fa-star-o"></i>\n' +
                        '                                    </div>\n' +
                        '                                    <div class="product-btns">\n' +
                        '                                        <button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span\n' +
                        '                                                class="tooltipp">add to wishlist</span>\n' +
                        '                                        </button>\n' +
                        '                                        <button class="add-to-compare" disabled><i class="fa fa-exchange"></i><span\n' +
                        '                                                class="tooltipp">add to compare</span>\n' +
                        '                                        </button>\n' +
                        '                                        <button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span>\n' +
                        '                                        </button>\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="add-to-cart">\n' +
                        '                                    <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart\n' +
                        '                                    </button>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>'
                });
                $(".PRODUCT_ID_RENDER").html(htmlOutput);
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });

    };

    index.allProduct();


});


function  countMyCard() {
    $.ajax({
        url: "/api/card/count",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            console.log(data);
            $('#qty_card').text(data.REC);
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}