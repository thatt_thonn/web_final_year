var index = {};

var myCurrentPage = 1;
var limit = 4;
var check = true;
var cate_id = 0;


$(document).ready(function () {

    if(authen_status==true){
        countWishlist();
        countMyCard();
    }


    $( "#select_category" ).change(function() {
        $('.input-checkbox').removeClass('ca_active');
        cate_id=$(this).val();
        index.allProduct(1, limit, cate_id);
    });


    $('.input-checkbox').click(function () {
        $('.input-checkbox').removeClass('ca_active');
        $(this).addClass('ca_active');
        cate_id = $(this).attr('data-id');
        index.allProduct(1, limit, cate_id);
    });


    /*** Function ***/
    index.allProduct = function (page, limit, cate_id) {
        $.ajax({
            url: "/api/product?limit=" + limit + "&page=" + page + "&categoryId=" + cate_id,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                console.log(data);
                var htmlOutput = '';
                data.REC.forEach(function (d) {
                    htmlOutput += '<div class="col-md-4 col-xs-6">\n' +
                        '                  <a href="/product/'+d.id+'">          <div class="product">\n' +
                        '                               <div class="product-img">\n' +
                        '                                     <img src="' + d.url + '" alt="">\n' +
                        '                                    <div class="product-label">\n' ;

                    if(d.is_advertise==true){
                        htmlOutput+=    '     <span class="new">NEW</span>\n';
                    }

                    htmlOutput+=
                        '                                    </div>\n' +
                        '                                </div></a>\n' +
                        '                                <div class="product-body">\n' +
                        '                                    <h3 class="product-name"><a href="/product/'+d.id+'">' + d.name + '</a></h3>\n' +
                        '                                    <h4 class="product-price">$' + d.price + '\n' +
                        '\n' +
                        '                                    </h4>\n' +
                        '                                    <div class="product-rating">\n' +

                        '                                    </div>\n' +
                        '                                    <div class="product-btns">\n';


                    if(d.add_to_card==0){
                        htmlOutput+='                                        <button class="add-to-wishlist" onclick="addToWishlist('+d.id+',this)"><i class="fa fa-heart-o"></i><span\n' +
                            '                                                class="tooltipp">add to wishlist</span>\n' +
                            '                                        </button>\n';
                    }else {
                        htmlOutput+='                                        <button class="add-to-wishlist" style="background-color: #E4E7ED; border-radius: 50%;" onclick="addToWishlist('+d.id+',this)"><i class="fa fa-heart-o" style="color:red !important;"></i><span\n' +
                            '                                                class="tooltipp">add to wishlist</span>\n' +
                            '                                        </button>\n';
                    }


                    htmlOutput+= '                                         <button class="quick-view" onclick="addToView('+d.id+',this)"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span>\n' +
                        '                                        </button>('+d.total_view+')\n' +
                        '                                    </div>\n' +
                        '                                </div>\n' +
                        '                                <div class="add-to-cart">\n' +
                        '                                    <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart\n' +
                        '                                    </button>\n' +
                        '                                </div>\n' +
                        '                            </div>\n' +
                        '                        </div>'
                });
                $(".PRODUCT_ID_RENDER").html(htmlOutput);
                // if (check) {
                index.setPagination(data.PAGENATION.total_pages, page);
                // }
                // }
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });

    };


    /** Pagination **/
    index.setPagination = function (totalPage, currentPage) {
        if (totalPage == 1) {
            $('#pagination-here').empty();
        } else {
            $('#pagination-here').empty();
            $('#pagination-here').bootpag({
                total: totalPage,
                page: currentPage,
                maxVisible: 6,
                leaps: true,
                firstLastUse: true,
                first: 'First',
                last: 'Last',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            }).on("page", function (event, currentPage) {
                // check = false;
                myCurrentPage = currentPage;
                index.allProduct(currentPage, limit, cate_id);
                $('html, body').animate({
                    scrollTop: $("#PAGE_SCROLL").offset().top
                }, 900);
            });
        }
    };

    // local_loadCategory();
    index.allProduct(myCurrentPage, limit, 0);


});


function local_loadCategory() {
    $.ajax({
        url: "/api/category",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            console.log("category: ", data);
            var first_item = '';
            if (data.STATUS == "0000") {
                data.REC.forEach(function (d) {
                    first_item += '<div class="input-checkbox" >\n' +
                        '                                <input type="checkbox" id="category-' + d.id + '">\n' +
                        '                                <label for="category-' + d.id + '">\n' +
                        '                                    <span></span>\n' +
                        '                                    ' + d.name + '\n' +
                        '                                    <small>(' + d.total_product + ')</small>\n' +
                        '                                </label>\n' +
                        '                            </div>';
                });
                $('#category_list').html(first_item);
            }
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}




function addToWishlist(productId,$this) {
    // /user/api/wishlist
    if(authen_status==true){
        $.ajax({
            url: "/api/wishlist/"+productId,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                if(data.REC==0){
                    $($this).find('i').removeAttr('style');
                    $($this).removeAttr('style');
                    countWishlist();
                }else if(data.REC==1){
                    $($this).find('i').removeAttr('style');
                    $($this).removeAttr('style');
                    countWishlist();
                }else if(data.REC==2){
                    $($this).find('i').css('color','red');
                    $($this).attr('style','background-color: #E4E7ED; border-radius: 50%;');
                    countWishlist();
                }
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });
    }else{
        window.location.href="/login";
    }


}


function addToView(productId,$this) {
    window.location.href="/product/"+productId;
}


function  countMyCard() {
    $.ajax({
        url: "/api/card/count",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            console.log(data);
            $('#qty_card').text(data.REC);
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}