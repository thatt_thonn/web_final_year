var index = {};

var myCurrentPage = 1;
var limit = 4;
var check = true;


var cate_id=0;


$(document).ready(function () {

    if(authen_status==true){
        countWishlist();
        countMyCard();
    }


    $( "#select_category" ).change(function() {
        cate_id=$(this).val();
        console.log(cate_id);
        $('#category_section').hide();
    });


    /*** Function ***/
    index.allProduct = function (page, limit) {
        $.ajax({
            url: "/api/product?limit=" + limit + "&page=" + page+"&wishlistUserId="+auth_user_id+"&categoryId=" + cate_id,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                if($(data.REC).length>0){
                    var htmlOutput = '';
                    data.REC.forEach(function (d) {
                        console.log(data);
                        htmlOutput += '<div class="col-md-3 col-xs-3">\n' +
                            '                    <div class="product">\n' +
                            '                        <a href="/product/'+d.id+'"> <div class="product-img">\n' +
                            '                            <img src="'+d.url+'" alt="">\n' +
                            '                            <div class="product-label">\n' +
                            '                                <span class="new">X</span>\n' +
                            '                            </div>\n' +
                            '                        </div></a>\n' +
                            '                        <div class="product-body">\n' +
                            '                            <h3 class="product-name"><a href="/product/'+d.id+'">'+d.name+'</a></h3>\n' +
                            '                            <h4 class="product-price">$'+d.price+'\n' +
                            '                                ' +
                            '                            </h4>\n' +

                            '                            <div class="add-to-carts">\n' +
                            '                                <button class=" btn-primary add-to-cart-btn primary-btn"><i\n' +
                            '                                        class="fa fa-shopping-cart"></i> add to cart\n' +
                            '                                </button>\n' +
                            '                                <!-- <button class="btn btn-lg btn-primary primary-btn order-submit btn-block btn-info" type="submit">Register</button> -->\n' +
                            '                            </div>\n' +
                            '                        </div>\n' +
                            '                    </div>\n' +
                            '                </div>';
                    });
                    $(".PRODUCT_ID_RENDER").html(htmlOutput);
                    if (check) {
                        index.setPagination(data.PAGENATION.total_pages, page);
                    }
                }else{
                    var htmlOutput='<div class="row"><div class="col-12 text-center"><h4>No wishlist product Items</h4></div></div>';
                    $(".PRODUCT_ID_RENDER").html(htmlOutput);
                }
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });

    };


    /** Pagination **/
    index.setPagination = function (totalPage, currentPage) {
        if (totalPage == 1) {
            $('#pagination-here').empty();
        } else {
            $('#pagination-here').empty();
            $('#pagination-here').bootpag({
                total: totalPage,
                page: currentPage,
                maxVisible: 6,
                leaps: true,
                firstLastUse: true,
                first: 'First',
                last: 'Last',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            }).on("page", function (event, currentPage) {
                check = false;
                myCurrentPage = currentPage;
                index.allProduct(currentPage, limit);
                $('html, body').animate({
                    scrollTop: $("#PAGE_SCROLL").offset().top
                }, 900);
            });
        }
    };

    // local_loadCategory();
    index.allProduct(myCurrentPage, limit);


});




function local_loadCategory() {
    $.ajax({
        url: "/api/category",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            console.log("category: ",data);
            var first_item=  '';
            if (data.STATUS == "0000") {
                data.REC.forEach(function (d) {
                    first_item+='<div class="input-checkbox" >\n' +
                        '                                <input type="checkbox" id="category-'+d.id+'">\n' +
                        '                                <label for="category-'+d.id+'">\n' +
                        '                                    <span></span>\n' +
                        '                                    '+d.name+'\n' +
                        '                                    <small>('+d.total_product+')</small>\n' +
                        '                                </label>\n' +
                        '                            </div>';
                });
                $('#category_list').html(first_item);
            }
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}



function  countMyCard() {
    $.ajax({
        url: "/api/card/count",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            console.log(data);
            $('#qty_card').text(data.REC);
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}



function countWishlist(){
    $.ajax({
        url: "/api/wishlist/count",
        type: "GET",
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Accept", "application/json");
            xhr.setRequestHeader("Content-Type", "application/json");
        },
        success: function (data) {
            $('#qty_wishlist').text(data.REC);
        },
        error: function (data, status, er) {
            console.log(data);
        }
    });
}